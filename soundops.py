from mysound import Sound
import unittest


def soundadd(s1: Sound, s2: Sound) -> Sound:
    # Calcular la duración del sonido resultante
    max_buffer_length = max(len(s1.buffer), len(s2.buffer))
    result_duration = max_buffer_length / Sound.samples_second

    # Crear el objeto Sound para el sonido resultante
    result_sound = Sound(result_duration)

    # Sumar los valores de los buffers de los sonidos de entrada
    for i in range(len(result_sound.buffer)):
        if i < len(s1.buffer) and i < len(s2.buffer):
            result_sound.buffer[i] = s1.buffer[i] + s2.buffer[i]
        elif i < len(s1.buffer):
            result_sound.buffer[i] = s1.buffer[i]
        elif i < len(s2.buffer):
            result_sound.buffer[i] = s2.buffer[i]

    return result_sound


class TestSoundOps(unittest.TestCase):

    def test_soundadd(self):
        # Crear dos sonidos de ejemplo
        s1 = Sound(1)
        s2 = Sound(1)

        # Agregar algunos valores a los buffers de los sonidos
        s1.buffer = [1] * len(s1.buffer)
        s2.buffer = [2] * len(s2.buffer)

        # Sumar los sonidos
        resultado = soundadd(s1, s2)

        # Verificar que el resultado tenga la longitud correcta
        self.assertEqual(len(resultado.buffer), len(s1.buffer))

        # Verificar que la suma se haya realizado correctamente
        expected_buffer = [3] * len(s1.buffer)
        self.assertEqual(resultado.buffer, expected_buffer)


if __name__ == '__main__':
    unittest.main()
