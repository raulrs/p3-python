import unittest
from mysound import Sound
from soundops import soundadd


class TestSoundAdd(unittest.TestCase):
    def test_equal_length_sounds(self):
        # Crear dos sonidos de igual longitud
        s1 = Sound(1)
        s2 = Sound(1)

        # Agregar valores a los buffers de los sonidos
        s1.buffer = [1] * len(s1.buffer)
        s2.buffer = [2] * len(s2.buffer)

        # Sumar los sonidos
        resultado = soundadd(s1, s2)

        # Verificar que la longitud del resultado sea correcta
        self.assertEqual(len(resultado.buffer),
                         max(len(s1.buffer), len(s2.buffer)))

        # Verificar que la suma se haya realizado correctamente
        expected_buffer = [3] * len(s1.buffer)
        self.assertEqual(resultado.buffer, expected_buffer)

    def test_different_length_sounds(self):
        # Crear dos sonidos de distinta longitud
        s1 = Sound(1)
        s2 = Sound(0.5)

        # Agregar valores a los buffers de los sonidos
        s1.buffer = [1] * len(s1.buffer)
        s2.buffer = [2] * len(s2.buffer)

        # Sumar los sonidos
        resultado = soundadd(s1, s2)

        # Verificar que la longitud del resultado sea correcta
        self.assertEqual(len(resultado.buffer),
                         max(len(s1.buffer), len(s2.buffer)))

        # Verificar que la suma se haya realizado correctamente
        expected_buffer = [3] * len(s2.buffer) + \
            [1] * (len(s1.buffer) - len(s2.buffer))
        self.assertEqual(resultado.buffer, expected_buffer)

    def test_empty_sound(self):
        # Crear un sonido vacío y uno con datos
        s1 = Sound(1)
        s2 = Sound(0)

        # Agregar valores a los buffers de los sonidos
        s1.buffer = [1] * len(s1.buffer)
        s2.buffer = []

        # Sumar los sonidos
        resultado = soundadd(s1, s2)

        # Verificar que la longitud del resultado sea correcta
        self.assertEqual(len(resultado.buffer),
                         max(len(s1.buffer), len(s2.buffer)))

        # Verificar que la suma se haya realizado correctamente
        expected_buffer = [1] * len(s1.buffer)
        self.assertEqual(resultado.buffer, expected_buffer)


if __name__ == '__main__':
    unittest.main()
