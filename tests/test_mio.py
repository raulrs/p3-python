import unittest
from tests.suma import sumar


class TestSumar(unittest.TestCase):
    def test_sumar(self):
        self.assertEqual(sumar(1, 2), 3)
        self.assertEqual(sumar(-1, 1), 0)
        self.assertEqual(sumar(0, 0), 0)
        self.assertEqual(sumar(-1, -1), -2)


# He probado cambiando el valor de 3 por 4 en el resultado
# de la primera suma, y este es el resultado que he obtenido
# en la terminal:
# FAILED (failures=1)
# 4 != 3
# Expected :3
# Actual   :4

if __name__ == '__main__':
    unittest.main()
