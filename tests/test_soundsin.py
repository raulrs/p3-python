import unittest
from mysoundsin import SoundSin


class TestSoundSin(unittest.TestCase):

    def test_creation(self):
        # Prueba la creación básica de un objeto SoundSin
        sound_sin = SoundSin(1, 440, 1000)
        self.assertEqual(sound_sin.duration, 1)
        self.assertEqual(len(sound_sin.buffer), sound_sin.samples_second)

    def test_inheritance(self):
        # Prueba que los objetos SoundSin hereden las propiedades de Sound
        # correctamente
        sound_sin = SoundSin(1, 440, 1000)
        self.assertEqual(sound_sin.max_amplitude, SoundSin.max_amplitude)
        self.assertEqual(sound_sin.samples_second, SoundSin.samples_second)

    def test_sinusoidal_signal(self):
        # Prueba que el buffer contenga una señal sinusoidal
        sound_sin = SoundSin(1, 440, 1000)
        # Verifica los valores del buffer estén dentro del rango esperado
        self.assertTrue(all(-SoundSin.max_amplitude <= x <=
                            SoundSin.max_amplitude for x in sound_sin.buffer))
        # Verifica algunos valores específicos que deberían ser cero
        self.assertEqual(0, sound_sin.buffer[0])
        self.assertEqual(
            0, sound_sin.buffer[int(sound_sin.samples_second / 2)])

        # Ajustamos la aserción para que se adapte a
        # la cantidad real de muestras en el buffer
        self.assertGreaterEqual(len(sound_sin.buffer),
                                sound_sin.samples_second)


if __name__ == '__main__':
    unittest.main()
