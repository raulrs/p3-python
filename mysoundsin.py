import math


class Sound:
    samples_second = 44100
    max_amplitude = 2 ** 15 - 1

    def __init__(self, duration):
        """
        Initializes a new instance of the class with the specified duration.

        @param duration: The duration of the sound in seconds
        @returns: None
        """
        self.duration = duration
        self.nsamples = int(self.samples_second * self.duration)
        self.buffer = [0] * self.nsamples


class SoundSin(Sound):
    def __init__(self, duration, frequency, amplitude):
        """

        :rtype: object
        """
        super().__init__(duration)  # Llamamos al constructor (Sound)
        for i in range(self.nsamples):
            t = i / self.samples_second
            # Dividimos la línea para cumplir con la regla de longitud máxima
            # de línea
            self.buffer[i] = int(
                amplitude * math.sin(2 * math.pi * frequency * t)
            )


# Utilizo este bloque de código para verificar que funciona el código anterior.
# El resultado que obtengo en la terminal es:
# First 10 values of the buffer:
# [0, 6, 12, 18, 24, 30, 36, 42, 48, 53]
if __name__ == "__main__":
    sound_sin = SoundSin(5, 440, 100)
    print("First 10 values of the buffer:")
    print(sound_sin.buffer[:10])
